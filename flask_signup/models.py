from datetime import datetime

from dateutil import rrule
from recurrent.event_parser import RecurringEvent
from sqlalchemy.sql import func

from flask_signup.extensions import db


class Page(db.Model):
    __tablename__ = "page"

    url = db.Column(db.String(), primary_key=True)
    timestamp = db.Column(db.String())
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now(), onupdate=func.now())

    items = db.relationship("Item", back_populates="page")
    posts = db.relationship("Post", back_populates="page")

    def _rrule(self):
        r = RecurringEvent(now_date=self.created_at)
        r.parse(self.timestamp)
        r.dtstart = self.created_at  # must be set explicitly for some reason
        return rrule.rrulestr(r.get_RFC_rrule())

    def next_time(self):
        time = self._rrule().after(datetime.now())
        return time

    def previous_time(self):
        time = self._rrule().before(self.next_time())
        return time

    def __repr__(self):
        return f"<Page {self.url}>"

    def __str__(self):
        return self.url


class Item(db.Model):
    __tablename__ = "item"

    id = db.Column(db.Integer, primary_key=True)  # noqa: A003
    name = db.Column(db.String())
    page_url = db.Column(db.String(), db.ForeignKey("page.url"))
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    page = db.relationship("Page", back_populates="items")

    def __repr__(self):
        return f"<Item {self.name}>"

    def __str__(self):
        return self.name


class Post(db.Model):
    __tablename__ = "post"

    id = db.Column(db.Integer, primary_key=True)  # noqa: A003
    message = db.Column(db.String())
    page_url = db.Column(db.String(), db.ForeignKey("page.url"))
    created_at = db.Column(db.DateTime(timezone=True), server_default=func.now())

    page = db.relationship("Page", back_populates="posts")

    def __repr__(self):
        return f"<Post {self.message}>"

    def __str__(self):
        return self.message
