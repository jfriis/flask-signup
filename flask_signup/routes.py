import datetime
from zoneinfo import ZoneInfo

from flask import Blueprint, current_app, make_response, render_template, request
from recurrent.event_parser import RecurringEvent

from flask_signup.extensions import db
from flask_signup.models import Item, Page, Post

blueprint = Blueprint("root", __name__)


def get_timezone():
    return ZoneInfo(current_app.config["TIMEZONE"])


@blueprint.route("/")
def root():
    return render_template("index.html")


@blueprint.route("/<prefix>")
def site(prefix):
    url = f"/{prefix}"
    page = db.session.execute(db.select(Page).where(Page.url == url)).scalar()
    if page:
        previous_time = page.previous_time()
        if previous_time:
            previous_time = page.previous_time().replace(tzinfo=ZoneInfo("UTC"))
            db.session.execute(db.delete(Item).where(Item.created_at < previous_time))
            db.session.execute(db.delete(Post).where(Post.created_at < previous_time))
            db.session.commit()
    return render_template("page.html", prefix=prefix, page=page)


@blueprint.route("/<prefix>/timestamp", methods=["GET"])
def get_timestamp(prefix):
    url = f"/{prefix}"
    page = db.session.execute(db.select(Page).where(Page.url == url)).scalar()
    return_form = request.args.get("form", "no")
    if return_form == "yes":
        return render_template("partials/timestamp_form.html", prefix=prefix, page=page)
    return render_template("partials/timestamp.html", prefix=prefix, page=page)


@blueprint.route("/<prefix>/timestamp", methods=["POST"])
def post_timestamp(prefix):
    url = f"/{prefix}"
    timestamp = request.form["timestamp"].strip()
    created_at = datetime.datetime.now(tz=get_timezone())
    page = db.session.execute(db.select(Page).where(Page.url == url)).scalar()
    if page:
        created = False
    else:
        created = True
        page = Page(url=url)
    page.timestamp = timestamp
    page.created_at = created_at
    r = RecurringEvent(now_date=page.created_at)
    rrule = r.parse(timestamp)
    if not rrule:
        # AttributeError: 'NoneType' object has no attribute 'replace'
        error = "Invalid time"
        return render_template("partials/timestamp_form.html", prefix=prefix, page=page, error=error)
    if created:
        db.session.add(page)
    db.session.commit()
    response = make_response(render_template("partials/timestamp.html", prefix=prefix, page=page))
    response.headers["HX-Refresh"] = "true"  # the whole page needs to be reloaded for page object
    return response


@blueprint.route("/<page>/item-create", methods=["POST"])
def item_create(page):
    url = f"/{page}"
    page = db.session.execute(db.select(Page).where(Page.url == url)).scalar()
    if not page:
        return ("", 204)
    name = request.form["create"].strip()
    item = Item(name=name, page_url=page.url)
    db.session.add(item)
    db.session.commit()
    return render_template("partials/item-form.html", page=page, item=item)


@blueprint.route("/<page>/item-delete", methods=["POST"])
def item_delete(page):
    item_id = request.form["delete"]
    db.session.execute(db.delete(Item).where(Item.id == item_id))
    db.session.commit()
    return ("", 200)  # 204 stops HTMX


@blueprint.route("/<page>/post-create", methods=["POST"])
def post_create(page):
    url = f"/{page}"
    page = db.session.execute(db.select(Page).where(Page.url == url)).scalar()
    if not page:
        return ("", 204)
    message = request.form["create"].strip()
    post = Post(message=message, page_url=page.url)
    db.session.add(post)
    db.session.commit()
    return render_template("partials/post-form.html", page=page, post=post)


@blueprint.route("/<page>/post-delete", methods=["POST"])
def post_delete(page):
    post_id = request.form["delete"]
    db.session.execute(db.delete(Post).where(Post.id == post_id))
    db.session.commit()
    return ("", 200)  # 204 stops HTMX
