import os


class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    TIMEZONE = os.environ.get("TZ", "UTC")
    SECRET_KEY = os.environ.get("SECRET_KEY", "this-really-needs-to-be-changed")
    SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
