import os

import jinja_partials
from flask import Flask

from flask_signup.extensions import db, migrate
from flask_signup.routes import blueprint


def create_app(config_object=None):
    if config_object is None:
        config_object = os.environ["APP_SETTINGS"]
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    return app


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    jinja_partials.register_extensions(app)


def register_blueprints(app):
    app.register_blueprint(blueprint)
