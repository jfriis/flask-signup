#!/bin/sh

docker stop flask-signup-postgres
docker container rm flask-signup-postgres
docker run -p 5433:5432 --name flask-signup-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
sleep 3s
. .venv/bin/activate
rm -rf migrations/
flask --app flask_signup.app db init
flask --app flask_signup.app db migrate -m "Initial migration."
flask --app flask_signup.app db upgrade
