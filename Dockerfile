# syntax=docker/dockerfile:1

FROM python:3.10-slim-buster

ENV POETRY_VERSION=1.3.2 \
    POETRY_VIRTUALENVS_CREATE=false

# Install poetry
RUN pip install "poetry==$POETRY_VERSION"

# Copy only requirements to cache them in docker layer
WORKDIR /code
COPY entrypoint.sh poetry.lock pyproject.toml /code/

# Project initialization:
RUN poetry install --no-interaction --no-ansi --no-root --no-dev

# Copy Python code to the Docker image
COPY flask_signup /code/flask_signup/
COPY migrations /code/migrations/

ENV SECRET_KEY=replace-this
ENV APP_SETTINGS=flask_signup.settings.ProductionConfig
ENV DATABASE_URL=postgresql://postgres:mysecretpassword@postgres:5432/postgres
ENV TZ=UTC

EXPOSE 8000

CMD ["./entrypoint.sh"]
