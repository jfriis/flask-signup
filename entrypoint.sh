#!/bin/sh
python -m flask --app flask_signup.app db upgrade
python -m gunicorn -b '0.0.0.0:8000' 'flask_signup.app:create_app()'
